# Gif Finder

Implementation of an app in Flutter that connects to the Giphy API to find and display the most famous Gifs of the moment. It also allows you to search for gifs and walk through the search.

It is possible to select a gif and share its url.

The application demonstrates in a simple way how to change screens in Flutter


# Sample screens
![enter image description here](https://gitlab.com/ariltonJAguilar/gif-finder/-/raw/master/samples/sample1.jpg)
![enter image description here](https://gitlab.com/ariltonJAguilar/gif-finder/-/raw/master/samples/sample2.jpg)

![enter image description here](https://gitlab.com/ariltonJAguilar/gif-finder/-/raw/master/samples/sample3.jpg)

